#include "Ippl.h"
#include "Random/Randn.h"
#include <random>
#include <vector>

#include "GasMan.h"


double domainSize;
double T1 = 300.0;
double T2 = 200.0;
double m = 6.6e-26; // Argon (kg)
std::mt19937 RNG_1(420);
Timer timer;

// Create save-file
std::ofstream saveQ("saveQ.csv");
std::ofstream saveT("saveT.csv");

void BCond(Particles& gas){
    BC_thermal(gas,0,0.0,false,k_B*T1,0.0,1);                     //left
    BC_thermal(gas,0,domainSize,true,k_B*T2,0.0,1);            //right
}

void integrate(Particles& gas, uint steps, uint updates = 10, bool talk=true, bool save=false){
    for (uint t = 0; t < steps; t++){
        if (talk && (t*updates) % steps == 0){
            msg << t << "/" << steps << endl;
        }

        p2c1D(gas);
        moveParticles(gas);
        BCond(gas);
        gas.update();

        if (save){
            momInteg1D(gas, saveQ, cellLayout[0], domainSize, "hFlux");
            momInteg1D(gas, saveT, cellLayout[0], domainSize, "temp");
        }
    }
    return;
}


// Initial setup
Particles initial(const size_t numP, Vector<double,Dim> domainSize){
    Particles gas(new Particles::Layout_t());
    gas.create(numP);
    
    std::uniform_real_distribution<double> uDist(0.0,1.0);
    double T_mean = (T1+T2)/2;
    
    for (size_t i = 0; i < numP; i++){
        gas.M(i) = m;
        gas.R(i) = Vector<double,Dim>(uDist(RNG_1), 0, 0) * domainSize;  // Random initial positions (normal dist)
        gas.V(i) = (Vector<double,Dim>(uDist(RNG_1), 0, 0) - Vector<double,Dim>(0.5,0,0)) //*4
                        *sqrt(-2*k_B*T_mean*log(uDist(RNG_1))/m);
    }
    gas.update();
    return gas;
}


int main(int argc, char* argv[]) {

    initialize(argc, argv);
    {
        msg << "---==={ Fourier flow }===---" << endl;
        
        // #steps | n | #cells(axis) | p/cell | tempLeft | tempRight | Kn | outMsg | momIntSteps
        // ./Fourier 2000 1e21 100 500 200 300 1 10 100
        
        // handle input
        const uint nt = std::atoi(argv[1]); // Number of time steps (3000 init, +samples)
        double avgNumDens = std::atof(argv[2]); // Number density (10^21 / m^3)
        cellLayout = {std::atoi(argv[3]),0,0}; // gridLayout
        const uint macrosPerCell = std::atoi(argv[4]); // Avg number of macroparticles per cell (500)
        T1 = std::atof(argv[5]); // wall temperature left
        T2 = std::atof(argv[6]); // wall temperature right
        const double Kn = std::atof(argv[7]); // Knudsen number [0.01,10]
        const int outSteps = std::atoi(argv[8]); // how many msg are passed to user
        const int momSteps = std::atoi(argv[9]); // how many steps to make during moment integration

        //sig_T = 1e-20; // Argon (~1eV)
        sig_T = 1 / (avgNumDens*Kn);
        double meanFreePath = 1 / (avgNumDens * sig_T);
        domainSize = meanFreePath / Kn;
        cellSize = domainSize / cellLayout[0];
        size_t nP = macrosPerCell * cellLayout[0];
        micPmac = avgNumDens * domainSize / nP;

        
        dT = 0.5*cellSize / sqrt(k_B*std::max(T1,T2) / (m));

        // Show user the parameters
        msg << "Time steps: " << nt << "   (dt=" << dT << ")" << endl;
        msg << "Grid: " << cellLayout << "   (cSize=" << cellSize << ")" << endl;
        msg << "DomainSize: " << domainSize << "(m)" << endl;
        msg << "#(macro)Particles: " << nP << "   (mic/mac=" << micPmac << ")" << endl;
        msg << "Temperatures: " << T1 << ", " << T2 << "(K)" << endl;
        msg << "Knudsen: " << Kn << "   (meanFreePath=" << meanFreePath << "(m))" << endl;
        msg << "---==========================---" << endl;
      
        // uniform initial distribution
        Particles gas = initial(nP,domainSize);
      

        msg << "integrating..." << endl;
        timer.start();
        integrate(gas, nt, outSteps);
        timer.stop();
        msg << "...done in: " << timer.elapsed() << "(s)" << endl;
        //csvExport(gas,"FourierTest",nP);
        integrate(gas,momSteps,10,true,true);

        //histo(gas,20,"pos",0);
        //histo(gas,20,"pos",1);
        //histo(gas,20,"velAbs",0);
        //histo(gas,20,"velAbs",1);


    }
    finalize();

    return 0;
}
