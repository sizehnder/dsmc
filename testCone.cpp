#include "Ippl.h"
#include "Random/Randn.h"
#include <random>
#include <vector>

#include "GasMan.h"

Vector<double,3> domainSize;
std::vector<double> w; // cell weights
double initVel = 1.0;
double T = 300.0;
double m = 6.6e-26; // Argon (kg)
std::mt19937 RNG_1(420);

// Create save-file
std::ofstream saveV("saveV.csv");
std::ofstream saveD("saveD.csv");

Timer timer;

// Fancy custom BC (note: initial particles may be "inside" cone)
void BC_cone(Particles& gas, double kT=1.0, double vel=0.0){
    Vector<double,3> nVec = {-1,2,0}; // normal vector
    Vector<double,3> pVec = {1,0.5,0}; // wall vector
    nVec /= norm(nVec);
    pVec /= norm(pVec);
    Kokkos::parallel_for("coneBC", gas.getTotalNum(), KOKKOS_LAMBDA(const size_t i){
        if (gas.R(i)[1] < (0.5*(gas.R(i)[0] - domainSize[0]/4))){ // if "inside" cone
            Vector<double,3> v = Vector<double,3>(gas.R(i)[0]- domainSize[0]/4,gas.R(i)[1],0);
            Vector<double,3> p = nVec.dot(v) * nVec;
            gas.R(i) -= 2*p;

            std::uniform_real_distribution<double> uDist(0.0,1.0);
            std::normal_distribution<double> nDist(vel,sqrt(kT/gas.M(i)));
            double v_new = sqrt(-2*kT*log(uDist(RNG))/gas.M(i)); // resampling function from slides

            gas.V(i) = v_new*nVec + nDist(RNG_1)*pVec;
        }
    });
}

void BCond(Particles& gas){

    BC_inflow(gas,{-cellSize,0.0,0.0,domainSize[1]},k_B*1000,3e20,m);  //left
    BC_inflow(gas,{domainSize[0],domainSize[0]+cellSize,(3.0/4.0)*domainSize[1],domainSize[1]},k_B*300,1e15,m); // right
    //BC_inflow(gas,{(1.0/8.0)*domainSize[0],(2.0/8.0)*domainSize[0],(1.0/8.0)*domainSize[1],(2.0/8.0)*domainSize[1]},k_B*500,5e20,m); // inside
    //gas.update();

    BC_cone(gas, k_B*T);
    
    BC_specular(gas,1,0.0,false);                                //bottom
    BC_specular(gas,1,domainSize[1],true);                       //top
    //BC_specular(gas,0,0.0,false);                                //left
    //BC_specular(gas,0,domainSize[0],true);                       //right

    //BC_voiding(gas,1,0,false);                                 //bottom
    //BC_voiding(gas,1,domainSize[1],true);                        //top
    BC_voiding(gas,0,0,false);                                 //left
    BC_voiding(gas,0,domainSize[0],true);                        //right
}

// for cell weights
void BC_reduced(Particles& gas){BC_cone(gas);}; // for cell weights

// Initial setup
Particles initial(const size_t numP, Vector<double,Dim> domainSize){
    Particles gas(new Particles::Layout_t());
    gas.create(numP);
    
    std::uniform_real_distribution<double> uDist(0.0,1.0);
    
    for (size_t i = 0; i < numP; i++){ // ========================= Access elements with gas.M(i)
        gas.M(i) = m;
        gas.R(i) = Vector<double,Dim>(uDist(RNG_1), uDist(RNG_1), 0) * domainSize;  // Random initial positions (normal dist)
        gas.V(i) = (Vector<double,Dim>(uDist(RNG_1), uDist(RNG_1), 0) - Vector<double,Dim>(0.5,0.5,0)) * initVel;
    }
    gas.update();
    return gas;
}

void integrate(Particles& gas, uint steps, uint updates = 10, bool talk=true){
    for (uint t = 0; t < steps; t++){
        if (talk && (t*updates) % steps == 0){
            msg << t << "/" << steps << " (" << gas.getTotalNum() << ")" << endl;
            //histo(gas,20,"pos",0,100.0);
            //histo(gas,20,"pos",1,100.0);
        }
        
        p2cRadial(gas,w.data());
        moveParticles(gas);
        BCond(gas);
        gas.update();
               
    }
    return;
}

void takeSamples(Particles& gas, uint samples = 10, uint stepsInbetween=1){
    msg << "Start sampling" << endl;
    for (uint i = 0; i < samples; i++)
    {
        if (i % 100 == 0){msg << i << "/" << samples << endl;}
        integrate(gas,stepsInbetween,1,false);
        momInteg2D(gas,saveV,{cellLayout[0],cellLayout[1]},{domainSize[0],domainSize[1]},"vel");
        momInteg2D(gas,saveD,{cellLayout[0],cellLayout[1]},{domainSize[0],domainSize[1]},"dens");
    }
    
}




int main(int argc, char* argv[]) {

    initialize(argc, argv);
    {
        msg << "---==={ Radial Cone Test }===---" << endl;
        
        // #steps | p/cell | initV | Kn  | #samples | outLogs
        // ./Cone 1000 100 300 1 0 10
        
        // handle input
        const uint nt = std::atoi(argv[1]); // Number of time steps
        const uint macrosPerCell = std::atoi(argv[2]); // Avg number of macroparticles per (500)
        initVel = std::atof(argv[3]);  // Lid speed (100, 300 m/s)
        const double Kn = std::atof(argv[4]); // Knudsen number [0.01,10]
        const int samples = std::atoi(argv[5]); // No of smaples (0=none)
        const int outSteps = std::atoi(argv[6]); // how many msg are passed to user

        sig_T = 7e-17; // Argon (~0.02eV ~ 300K) Fig.5 // https://iopscience.iop.org/article/10.1088/0953-4075/33/16/303/pdf
        double avgNumDens = 1e20; // Number density (10^21 / m^3)
        cellLayout = {100,50,0};
        
        double meanFreePath = 1 / (avgNumDens * sig_T);
        double L = meanFreePath / Kn;
        cellSize = L / cellLayout[0];
        domainSize = cellSize * cellLayout;
        size_t nP = macrosPerCell * cellLayout[0] * cellLayout[1];
        micPmac = avgNumDens * domainSize[0]*domainSize[1] / nP;

        if (cellSize > meanFreePath){ // If kept with 100x100 and Kn>=0.01 this will not happen
            msg << "[WARNING]: CellSize is bigger than MeanFreePath" << endl;
        }

        dT = 0.5*std::min(cellSize,meanFreePath) / std::max(sqrt(k_B*T / m),initVel);
        g_max = 3*std::max(sqrt(k_B*T / m),initVel);
        double nCandExp = macrosPerCell*macrosPerCell/(cellSize*cellSize) * g_max * sig_T * micPmac * dT;

        // Show user the parameters
        msg << "Time steps: " << nt << "   (dt=" << dT << ")" << endl;
        msg << "Grid: " << cellLayout << "   (cSize=" << cellSize << ")" << endl;
        msg << "DomainSize: " << domainSize << "(m)" << endl;
        msg << "#(macro)Particles: " << nP << "   (mic/mac=" << micPmac << ")" << endl;
        msg << "Temperature: " << T << "(K)" << endl;
        msg << "Init velocity: " << initVel << "(m/s)" << endl;
        msg << "Knudsen: " << Kn << "   (meanFreePath=" << meanFreePath << "(m))" << endl;
        msg << "N_cand expected (min): " << nCandExp << endl;
        msg << "N_cand expected (max): " << nCandExp*(pow(cellLayout[1],2) - pow(cellLayout[1]-1,2)) << endl;
        msg << "Console updates: " << outSteps << endl;
        msg << "---==========================---" << endl;

        msg << "Building cell weights..." << endl;
        w = cellWeights2D(BC_reduced);
        //for (int i = 0; i < (cellLayout[0] * cellLayout[1]); i++){msg << w[i] << "|";}

        // uniform initial distribution
        msg << "Initializing gas..." << endl;
        Particles gas = initial(nP,domainSize);
        
        msg << "Integrating..." << endl;
        timer.start();
        integrate(gas, nt, outSteps);
        timer.stop();
        msg << "...done in: " << timer.elapsed() << "(s)" << endl;
        timer.start();
        if (samples > 0){
            takeSamples(gas,samples,1);
        }
        timer.stop();
        msg << "DONE, total time: " << timer.elapsed() << "(s)" << endl;

    }
    finalize();

    return 0;
}