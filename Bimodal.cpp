#include "Ippl.h"
#include "Random/Randn.h"
#include <random>
#include <vector>

#include "GasMan.h"

// #time steps | #particles | gridX
// ./Bimodal 10 1000 64 --info 10
// ./Bimodal 10 3 4 --info 10

void integrate(Particles& gas, unsigned int steps){
    for (unsigned int t = 0; t < steps; t++){
        if ((t*10) % steps == 0){
            msg << t << "/" << steps << endl;
        }
        p2c1D(gas);
        moveParticles(gas);
        //boundaryConditions(gas, 0, "periodic", Vector<double, 2>(0, 1)); // Periodic BC in x-direction
        BC_periodic(gas,0,Vector<double, 2>(0, 1)); // updated to newer BC version
        //printParticles(gas,3);
        gas.update();
    }
}

Particles generateInitial(const detail::size_type numP, Vector<double,Dim> domainSize, std::string dist){
    Particles gas(new Particles::Layout_t());
    gas.create(numP);
    
    std::uniform_real_distribution<double> uDist(0.0,1.0);
    
    if (dist == "uniform"){
        for (size_t i = 0; i < numP; i++){ // ========================= Access elements with gas.M(i)
            gas.M(i) = 1.0;
            // [i] should access an element and (x) should give the axis
            gas.R(i) = Vector<double,Dim>(uDist(RNG), uDist(RNG), uDist(RNG)) * domainSize;  // Random initial positions (normal dist)
            //gas.V(i) = Vector<double,Dim>(nDist(RNG), nDist(RNG), nDist(RNG));  // Random initial velocities (normal dist)
            gas.V(i) = (Vector<double,Dim>(uDist(RNG), uDist(RNG), uDist(RNG)) - Vector<double,Dim>(0.5,0.5,0.5)) * domainSize;
        }
    }

    if (dist == "bimodal"){
        std::normal_distribution<double> mode1dist(-1.0, 0.5);
        std::normal_distribution<double> mode2dist(1.0, 0.5);
        for (size_t i = 0; i < numP; i++){ // ========================= Access elements with gas.M(i)
            gas.M(i) = 1.0;
            // [i] should access an element and (x) should give the axis
            gas.R(i) = Vector<double,Dim>(uDist(RNG), uDist(RNG), uDist(RNG)) * domainSize;  // Random initial positions (uniform dist)
            
            bool isEven = i % 2 == 0;

            // Bimodal as combination of two normal dist with means ± 1.
            gas.V(i) = isEven * mode1dist(RNG) + !isEven * mode2dist(RNG);
        }
        
    }
    
    gas.update();

    return gas;
}


int main(int argc, char* argv[]) {

    initialize(argc, argv);
    {
        msg << "---==={ Bimodal relaxation 1D }===---" << endl;
        
        // handle input
        const unsigned int nt = std::atoi(argv[1]); // Number of time steps
        const detail::size_type nP = std::atoll(argv[2]); // Number of Particles (as long integer)
        cellLayout = {std::atoi(argv[3]),0,0}; // gridLayout
        cellSize = 1/(double)cellLayout[0];
        Vector<double,3> domainSize = cellLayout*cellSize;
        //double meanFreePath = nP/sig_T*nDens; // Timon: have to exclude this since it has been redefined

        dT = 1e-5; // change later
        nDens = 1e20; // change later
        sig_T = 7e-17; // change later
        micPmac = nDens/nP;
        g_max = 2*2;

        msg << "Time steps: " << nt << endl;
        msg << "No of Particles: " << nP << endl;
        msg << "Cell grid: " << cellLayout << endl;
        msg << "Domain size: " << domainSize << endl;
        //msg << "Mean free path: " << meanFreePath << endl;
      
        Particles gas = generateInitial(nP, domainSize, "bimodal");

        //histo(gas,20,"pos",0);
        //histo(gas,20,"pos",1);
        histo(gas,20,"vel",0);
        csvExport(gas, "testInit");
        // histo(gas,20,"vel", 1);

        msg << "integrating..." << endl;
        integrate(gas, nt);
        msg << "...done" << endl;

        //histo(gas,20,"pos",0);
        //histo(gas,20,"pos",1);
        histo(gas,20,"vel",0);
        csvExport(gas, "testAfter");
        // histo(gas,20,"vel",1);

    }
    finalize();

    return 0;
}