\documentclass[10pt,a4paper,twoside]{tau-book}
\usepackage[english]{babel}
\usepackage{tau}
\usepackage{xfrac}
\newcommand{\landO}{\mathcal{O}}
\newcommand{\Kn}{\operatorname{Kn}}
\usepackage[ruled]{algorithm2e}
\usepackage{amssymb}
\usepackage{subcaption}
\usepackage[free-standing-units=true]{siunitx} % for SI units
%----------------------------------------------------------
% Title
%----------------------------------------------------------

\title{Direct Simulation Monte Carlo for Rarefied Gas}

%----------------------------------------------------------
% Authors, affiliations and professor
%----------------------------------------------------------

\author[*]{Timon Frank}
\author[*]{Hrvoje Krizic}
\author[*]{Simon Zehnder}

%----------------------------------------------------------

\affil[*]{Department of Physics, ETH Zurich}
\professor{Supervisor: Mohsen Sadr, <mohsen.sadr@psi.ch>.}

%----------------------------------------------------------
% Footpage notes
%----------------------------------------------------------

\institution{ETH Zürich}
\ftitle{Project 3}
\theday{\today} % \today
% \etal{et al.}
\course{Computational Statistical Physics}

%----------------------------------------------------------

\begin{document}
	
    \maketitle
    \thispagestyle{firststyle}

%----------------------------------------------------------
% Abstract
%----------------------------------------------------------


%----------------------------------------------------------

\begin{abstract}
This report presents a study on rarefied gas dynamics, covering theory, computational methods, and practical applications.
Theoretical insights highlight the Boltzmann equation's role in gas modeling and the significance of the Knudsen number ($\Kn$) in distinguishing between continuum and rarefied regimes.
The Direct Simulation Monte Carlo (DSMC) algorithm is introduced for simulating rarefied gas behavior efficiently, emphasizing collision event modeling and algorithmic implementation.
Boundary conditions and verification strategies are discussed to ensure simulation accuracy across various scenarios, validated through 1D and 2D simulations.
\end{abstract}

%----------------------------------------------------------
% Table of contents (Uncomment to use)
%----------------------------------------------------------

% \tableofcontents

%---------------------------------------------------------

\section{Introduction}
\subsection{Kinetic theory}
There exist two different ways to describe a gas flow. Firstly, a macroscopic model can be used to model the gas as a continuum, which is described accurately by the Navier-Stokes equations. Secondly, a microscopic (molecular) model can be chosen to ideally describe the state of each individual molecule at all times. Mathematically, the Boltzmann equation
\begin{equation}
    \frac{\partial f}{\partial t} + \nabla_x(vf) = S[f]
\end{equation}
is the appropriate model at the microscopic level, where $S[f]$ is the binary collision operator and $f(v|x,t)$ is the monatomic single-particle velocity distribution function. \\
In order to have a quantitative measure to distinguish between the macroscopic and molecular model, the Knudsen number
\begin{equation}
    \Kn \equiv \frac{\lambda}{L}
\end{equation}
is defined with the mean free path $\lambda = (\sigma_T n)^{-1}$ and the length scale of the problem $L$. The Knudsen number describes the degree of rarefaction of the gas. Generally, a Knudsen number of less than $0.1$ is required for the Navier-Stokes equation to be valid. Above this value, the errors get significantly higher and the molecular model should be applied. In this work, the focus lies on studying the behaviour of rarefied gas using the molecular model. \\
It is also useful to know the mean collision time of the gas in order to define the time steps of the simulation sufficiently small. The mean collision time is given by 
\begin{equation}
    t_{col} = \frac{1}{n\overline{\sigma_T v_r}}
\end{equation}
where $v_r = v_1 - v_2$ denotes the relative velocity, $\sigma_T$ is the total cross section and $n$ is the number density.
Furthermore, a 2D supersonic flow is studied, where the Mach number Ma is 
\begin{equation}
    \text{Ma} \equiv \frac{u}{c} > 1
\end{equation}
Here, $u$ denotes the local flow velocity with respect to the boundary and $c$ is the speed of sound in the gas. The speed of sound can be found with
\begin{equation} \label{eq:speedOfSound}
    c = \sqrt{\frac{\gamma k_B T}{m}}
\end{equation}
where $\gamma=5/3$ is the adiabatic index for a monoatomic gas, $T$ the temperature and $m$ the atomic mass.

\subsection{Macroscopic quantities}
From the single-particle velocity distribution function $f(v|x,t)$, it is possible to obtain the macroscopic properties of the gas. The number density is calculated by solving the integral
\begin{equation}
    n(x,t) = \int f(v|x,t)\,dv.
\end{equation}
Multiplying this by the molecular mass $m$ yields the mass density $\rho$. The first moment of the distribution yields the bulk velocity given by
\begin{equation}
    U_i(x,t) = \frac{1}{n(x,t)}\int v_i f(v|x,t)dv
\end{equation}
The temperature is given by
\begin{equation}
    T = \frac{m}{3n(x,t)k_B}\sum_{i=1}^3 \int (v_i')^2f(v|x,t) dv
\end{equation}
where $v' \equiv v - U$ is the fluctuating velocity. Another interesting quantity to study is the heat flux, which is obtained from
\begin{equation}
    q_i(x,t) = \frac{1}{2n(x,t)}\int v_i'\bigg( \sum_j v_j'^2 \bigg) f(v|x,t)dv
\end{equation}
Finally, the pressure tensor is defined as
\begin{equation}
    \pi_{ij}(x,t) = \frac{1}{n(x,t)}\int v'_i v'_j f(v|x,t) dv
\end{equation}
which allows calculating the shear stress using
\begin{equation}
    \tau_{ij} = \pi_{ij} - \frac{1}{3}\text{Tr}(\pi_{ij})\mathbb{1}
\end{equation}


\subsection{Collision rule}
In rarefied gases the interactions between the molecules are mainly binary collisions involving only two particles. Considering an elastic collision between molecules with initial velocities $\vec{v}_1, \vec{v}_2$, the post-collision velocities $v^*_1, v^*_2$ can easily be computed by considering energy and momentum conservation. The resulting velocities are
\begin{align} \label{eq:collVel}
    v_1^* &= v_m + \frac{m_2}{m_1 + m_2}v_r \\
    v_2^* &= v_m - \frac{m_1}{m_1 + m_2}v_r
\end{align}
where 
\begin{align} \label{eq:COMvel}
    v_m = \frac{m_1v_1 + m_2v_2}{m_1 + m_2}
\end{align}
and the deflection angle is given by $\chi = \pi - 2\theta_A$, where
\begin{equation}
    \theta_A = \int_0^{W_1} \sqrt{1-W^2- \frac{2\phi}{m_rv_r^2}} dW
\end{equation}
with the reduced mass $m_r$, the intermolecular potential $\phi$ and $W_1 = +\sqrt{1 - \frac{2\phi}{m_rv_r^2}}$. For the hard sphere model (HS model) it can be shown that
\begin{equation}
    \theta_A = \arcsin{\frac{b}{d_{12}}}
\end{equation}
where $d_{12} = \frac{1}{2}(d_1 + d_2)$ is the averaged diameter of the molecules and $b$ the impact parameter. 

For the simple dilute gas considered here, the Boltzmann collision factor $S[f]$ from the Boltzmann equation can be written as
\begin{equation}
    S[f(v)] = \int (f(v^*)f(v_1^*) - f(v)f(v_1))v_r\sigma d\Omega dv_1
\end{equation}
Solving this equation will then give the velocity distribution of our rarefied gas.

\section{DSMC}
The Direct Simulation Monte Carlo (DSMC) algorithm is a computational technique used to simulate the behavior of such rarefied gases described in the theory section. The simulation progresses in discrete time steps, and the simulation domain is discretized into cells with volume $V_C$. The gas flow is represented by a large number of computational particles. Each particle represents a certain number $F_N$ of gas molecules. These particles move through the simulation domain, interacting with each other and with the boundaries of the domain, described in the next section. The particles travel initially in straight lines between collisions with other particles or with boundaries. When two particles come within a certain distance of each other, or when a particle collides with a boundary, collision events are simulated. During these events, properties such as momentum, energy, and species may be changed. 
\subsection{Collisions}
The probability $P$ of collision between two simulated molecules with relative speed $v_r$ over the time interval $\Delta t$ is given by (see \cite{bird} for reference)
\begin{equation}\label{eq:prob1}
    P=\frac{F_N\sigma_Tv_r\Delta t}{V_C}.
\end{equation}
where $\sigma_T$ is the total cross-section of the two molecules. While it is theoretically possible to calculate collisions between all pairs of simulated molecules within the cell, it becomes inefficient due to the large number of pairs $N(N-1)/2$ and the (in general) small probabilities of collision. To solve this issue, only a fraction of the pairs are considered, and the resultant probability is adjusted accordingly by dividing equation (\ref{eq:prob1}) by this fraction. Therefore, we relate this probability to a maximum probability. The maximum efficiency is achieved if the probability is maximized. This is given by the following quantity:
\begin{equation}
    P_{\max} = \frac{F_N(\sigma_Tv_r)_{\max} \Delta t}{V_C}.
\end{equation}
The number of pair selections per time step is given by
\begin{equation}
    \frac{\text{no. of pair selections}}{\Delta t} = P_{\max} \cdot \frac{N^2}{2}
\end{equation}
Since $N$ (number of comp. particles in a cell) is a fluctuating quantity we can achieve better efficiency by taking $N\overline{N}$ instead of $N^2$, which yields
\begin{equation}
\begin{split}
    \frac{\text{no. of pair selections}}{\Delta t} &= P_{\max} \cdot \frac{N\overline{N}}{2}\\ &= \frac{N\overline{N}F_N(\sigma_Tv_r)_{\max} \Delta t}{2V_C} \\&= \frac{nN}{2}(\sigma_Tv_r)_{\max} \Delta t
\end{split}
\end{equation}
where we used the fact, that $\frac{\overline{N}F_N}{V_C}$ is just the number density $n$ of the flow. The collision probability in this NTC method is given by
\begin{equation}
    \frac{\sigma_Tv_r}{(\sigma_Tv_r)_{\max} },
\end{equation}
The computing time is $\landO(n)$. The parameter $(\sigma_Tv_r)_{\max}$ should be stored for each cell, initially set to a large value, and automatically updated during sampling. This parameter ensures that the collision rate remains accurate, unaffected by its exact value. 
\subsection{Algorithm}
The algorithm of the procedure discussed in the two previous sections is given.
\begin{algorithm}
\caption{DSMC algorithm}\label{alg:cap}
Initialize $\mathbf{X}, \mathbf{V} \sim f(\mathbf{v}|\mathbf{x}, t=0)$\;
\While{$t < t_\textit{final}$}{
\For{\textit{cells} $=1,\hdots ,N_\textit{cells}$}{
    $N_\textit{Cand} = \frac{1}{2}n N_{p/\textit{cell}}\max(\sigma_Tg)\Delta t$\;
    \For{$j=1,\hdots,N_{\textit{Cand}}$}{
        Pick two particles from the cell\;
        Draw $\xi \sim \mathcal{U}[0,1]$\;
        \If{$\sigma_Tv_r/\max(\sigma_Tv_r)>\xi$}{
            Draw $\varepsilon \in [0,2\pi]$ and calculate $\chi$ given molecular potential $\phi$ (HS)\;
            Collide selected particles following collision rule\;
        }
    }
}
}
\end{algorithm}
    \subsection{Boundary conditions}
        Boundary conditions are what define the problems at hand, they are used to model objects placed into a flow and are a necessity since our simulations are always finite in size. There are three main boundary conditions which will be used here:
    
        \begin{itemize}
            \item \textbf{Periodic} boundary conditions simply connect one side to the opposite side by transporting any particle leaving the system to the other side, where it re-enters.
            \item \textbf{Reflective} boundary conditions, also called specular walls, mirror the position and velocity of a particle leaving the system along the wall normal, such that it appears to bounce off the wall fully elastic.
            \item \textbf{Thermal} walls are used to simulate a wall that is fully diffusive and at equilibrium. The velocity $v_{\perp}$ parallel to the wall normal of a particle leaving the simulation is resampled from a Boltzmann distribution (\ref{eq:thermWall}), where $T_w$ is the wall temperature, $m$ the particle mass and $\mathcal{U}(0,1)$ a uniform distribution between 0 and 1. And the parallel velocity $v_{||}$ is resampled from a normal distribution $\mathcal{N}(v_s,\sqrt{k_BT / m})$ where $v_s$ is the boundary shearing velocity. The new perpendicular velocity is given by
        \end{itemize}
        \begin{equation}\label{eq:thermWall}
            v_{\perp} = \pm \sqrt{-\frac{2k_BT_w}{m}\log(\mathcal{U}(0,1))}
        \end{equation}
    
        Note that while periodic and reflective boundary conditions conserve energy, the thermal wall does not and instead gradually changes the energy of the particle gas until it is at equilibrium with the wall.
    
    \subsection{Verification and test cases}
        To check whether our implementation produces physically accurate simulations, we will test it on simple, well-understood systems. All test cases will be verified with a wide range of Knudsen numbers covering the entire rarefied gas regime.
        
        Firstly, we check if our simulation obeys the law of energy conservation and the Boltzmann velocity distribution inside a 1D box with periodic/reflective boundaries.
        
        After that, still in 1D, two walls performing a shearing movement are simulated. This will produce a Couvette flow \cite{bird} and allows us to measure shear stress. Similarly, we try to simulate conduction in the form of a Fourier flow \cite{bird} by measuring the heat flux between a thermal wall and the other side. Note that we test these systems for $\Kn\in[0.01,10]$
    
        In 2D, we will simulate a lid-driven cavity, which is a 2D box with 3 fixed walls and 1 wall performing a shearing movement. This system will be tested for $\Kn\in[0.01,10]$ and at different Mach numbers $\text{Ma}\in[1,10]$.

        Additionally, the two-dimensional flow around a box is simulated.
    
        %Lastly, we try to reproduce two real-life measurements \cite{doubleCone} of fast flows over double cones to verify shock interaction at high Mach numbers.

\section{Implementation}
    To achieve an easily scalable simulation we use the Independent Parallel Particle Layer (IPPL) developed at PSI. For now the implementation prioritizes a multi-threaded application and was not yet tested with GPU and distributed applications in mind.

    \subsection{Particles}
        We make use of the IPPL built in \verb|ParticleBase| class. The standard attributes of a particle involve an ID and a 3d-position vector $x$. To this we add a 3d-velocity $v$ and a mass $m$. The particle radius is not stored as a particle parameter as we only need it for the particle cross-section.

        The particles are created and given initial parameters according to the simulation parameters. They move according to
        \begin{equation} \label{eq:posEvolution}
            x' = x + v\Delta t
        \end{equation}
        with the velocity being updated by collision events.
        
    \subsection{P2C}
        To determine which particle is in which cell we implemented a function called particle to cell (P2C). There the position of a particle is multiplied by the number of cells and cast to an integer. The resulting number is used as the index for an array made of vectors to which the corresponding particle index is pushed. This results in a vector for each cell containing the indices of the particles in that cell.

        Each cell can then process it's collisions individually according to the DSMC algorithm (see Alg.\ref{alg:cap}). This allows for massive parallelization. We differentiate between 1D and 2D P2C since making it dimensionally independent is non-trivial.

        Note that we also tried using a binary-/quadTree and later a hash-map to perform P2C, but in the end found, that the most efficient (and easiest) is the current method using fixed arrays.

    \subsection{Collision}
        The parameter $g$ in the DSMC algorithm is found by comparing each particle to all the others. This is an $\mathcal{O}(N_{cell}^2)$ operation but permitted since $N_{cell}$ stays small.
        
        For now, only the hard-sphere collision is implemented. Because the DSMC algorithm doesn't care about the impact parameter $b$ for a collision to happen, a different method is used. The collision follows the equations (\ref{eq:collVel}-\ref{eq:COMvel}). However the direction of $v_r$ is drawn from a uniform distribution on a unit-sphere $\mathcal{SU}(1)$ with the magnitude calculated as usual.

    \subsection{Simulation step}
        Each simulation step is made up multiple processes.
        \begin{enumerate}
            \item P2C is used to determine the particles in each cell.
            \item For each cell the collisions are performed with the DSMC algorithm.
            \item The particles are moved according to equation (\ref{eq:posEvolution}).
            \item Boundary conditions (BC) are applied.
        \end{enumerate}
        Most boundary setups can be made by simply combining several of the implemented BC. Lastly all particle attributes are updated.

    \subsection{Parallelization}
        There are four main steps which make sense to be parallelized: Initialization, P2C, DSMC and BC.
        \begin{itemize}
            \item \textbf{Initialization:} Since the initialization is rather light-weight and different on a problem-by-problem basis, we did not parallelise it yet.
            \item \textbf{P2C:} This needs to be done for every particle, thus it could be nicely parallelised. Since our approach uses a \verb|push_back| operation this is sadly not possible for now.
            \item \textbf{DSMC:} Each cell can be worked on individually, which makes it ideal to parallelise.
            \item \textbf{BC:} This is also a per particle operation and benefits from parallelisation.
        \end{itemize}
        The parallelisation is achieved using the \verb|Kokkos::parallel_for| which automatically distributes the workload of a for loop. By using different kind of IPPL environements one can choose between single-threaded, multi-threaded and GPU computation. The later of which has not been tested yet. IPPL is created on-top of the Kokkos library, which makes our code cross-compatible for all the mentioned use-cases.

\section{Results}
    \subsection{Bimodal}
As a first example to test our model, the relaxation of a bimodal distribution in one dimension was simulated. The simulation ran with $10^4$ particles on a grid with 64 cells and a length of 1cm. The initial and final distribution of the velocities with periodic boundary conditions are shown in figure \ref{fig:bimodal_periodic}.
\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{Imgs/Bimodal_Periodic.png}
    \caption{Bimodal relaxation in 1D with periodic boundary conditions}
    \label{fig:bimodal_periodic}
\end{figure}
The final distribution corresponds to the expected Maxwell-Boltzmann distribution centered around zero. \\
The simulation was repeated for thermal boundary conditions with $T = 300K$. The results shown in figure \ref{fig:bimodal_thermal} are also in agreement with the expected final distribution. 
\begin{figure}[H]
    \centering
    \includegraphics[width=\columnwidth]{Imgs/Bimodal_Thermal.png}
    \caption{Bimodal relaxation in 1D with thermal bounary conditions}
    \label{fig:bimodal_thermal}
\end{figure}

    %\subsection{Fourier flow}

    \subsection{Couette flow}
    \label{couvette}
    After implementing boundary conditions for thermal walls, the DSMC method was tested in 1D test cases with moving walls as boundary conditions. A Couvette flow was simulated, where a gas flows between two parallel surfaces, one stationary and the other moving at a constant velocity ($v=$ \SI{300}{m\per s}). Initially, both walls and the gas have the same temperature ($T=273$ \SI{}{K}). Following the procedure in Bird \cite{bird}, the walls are separated by a distance of $L=$\SI{1}{\meter}, and the gas used is argon with a number density of $n= $\SI{1.4e20}{}. The simulation was set to a mean free path of \SI{0.01}{\meter}. This gives a Knudsen number of \SI{0.01}{} based on the surface spacing. The velocity of the outer surface is \SI{300}{m\per s}. Figure \ref{fig:velocity_couvette} shows a linear velocity increase. A linear function $f_{\text{fit.}}(T)=g\cdot T$ was fitted to obtain the gradient
    \begin{equation*}
        g = \SI{286(4)}{\meter \per \kelvin}.
    \end{equation*}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Velocity_Shear.png}
        \caption{Velocity versus position diagram of the Couvette flow test case with $v_{\text{wall}}=$\SI{300}{m\per s}. and $\Kn = 0.1$. A linear fit was used to calculate the gradient $g= \SI{286(4)}{\meter \per \kelvin}.$}
        \label{fig:velocity_couvette}
    \end{figure}
    The temperature profile is shown in figure \ref{fig:temperature_couvette}.
    \begin{figure}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Temperature_Shear.png}
        \caption{Temperature profile of the Couvette flow test case with $v_{\text{wall}}=$\SI{300}{m\per s} and $\Kn = 0.1$.}
        \label{fig:temperature_couvette}
    \end{figure}
    Furthermore, the shear stress was extracted. The shear stress in the case of a Couvette flow in $x-y$-direction is shown in figure \ref{fig:shearstress_couvette}. The shear stress can be extracted by averaging over all points, getting:
    \begin{equation*}
        \tau_{xy} = \SI{0.0062\pm 0.0002}{\newton \per \meter^2}.
    \end{equation*}
    \begin{figure}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Shearstress_Shear.png}
        \caption{Shear stress of the Couvette flow with parameters given in section \ref{couvette}}
        \label{fig:shearstress_couvette}
    \end{figure}
    
    \subsection{Lid-driven cavity}
        The lid-driven cavity is a 2D square box with thermal walls, the top one is shearing to the right with a velocity $v_s$. The wall temperature was set to $T=$ \SI{300}{K}. The particles have a number density $n=10^{20} $ \SI{}{m^{-3}} and the atomic mass $m$ of argon. The cross-section $\sigma$ is set such that $\sigma = 1 / \Kn L n$ with $L=$\SI{1}{m} being the box width. For the simulation, a 75x75 cell grid with 300 particles per cell was used. The time step $\Delta t = \Delta h / 2 \sqrt{k_BT/m}$ is chosen dependent on the wall temperature $T$ and cell-size $\Delta h$ \cite{TopCavity}. The speed of sound $c$ in this setup is calculated with equation (\ref{eq:speedOfSound}) to be \SI{323.3}{m/s}.
        
        We use 4000 steps to get the simulation into a stable state and then sample the simulation every 100 steps until we reach 5000 steps, giving us a total of 18'562'500 samples. The temperature map is smoothed with bicubic interpolation.
        
        \begin{figure}[H]
            \centering
            \begin{subfigure}{.75\columnwidth}
                \centering
                \includegraphics[width=\columnwidth]{Imgs/L300Kn005.png}
                \caption{$v_s=$ \SI{300}{m/s}, $\Kn=0.05$ \\ (heat-flux scaled by 2.5x)}
                \label{fig:LDC_M1_005}
            \end{subfigure}
            \begin{subfigure}{.75\columnwidth}
                \centering
                \includegraphics[width=\columnwidth]{Imgs/L300Kn02.png}
                \caption{$v_s=$ \SI{300}{m/s}, $\Kn=0.2$}
                \label{fig:LDC_M1_02}
            \end{subfigure}
            \begin{subfigure}{.75\columnwidth}
                \centering
                \includegraphics[width=\columnwidth]{Imgs/L300Kn1.png}
                \caption{$v_s=$ \SI{300}{m/s}, $\Kn=1$}
                \label{fig:LDC_M1_1}
            \end{subfigure}
            \caption{Temperature and heat-flux inside a lid-driven cavity for $\text{Ma}=0.93$}
            \label{fig:LDC_M1}
        \end{figure}
        
        We observe the shift in the heat-flux direction for higher Knudsen numbers and an increase in the heat-flux (see Fig. \ref{fig:LDC_M1}) as temperature gradients rise. Also clearly visible is the violation of Fouriers law caused by the non-equilibrium system. These results mimic the behaviour seen in \cite{TopCavity}.
        
        For higher Mach numbers (see Fig. \ref{fig:LDC_highMach}), temperatures rise and the heat-flux increases rapidly, while the cone of the heat-flux narrows and the non-equilibrium effects become more prominent.
        \begin{figure}[H]
            \centering
            \begin{subfigure}{.75\columnwidth}
                \centering
                \includegraphics[width=\columnwidth]{Imgs/L1000Kn1s.png}
                \caption{$v_s=$ \SI{1000}{m/s} (Ma=3.09), $\Kn=1$}
                \label{fig:LDC_M3}
            \end{subfigure}
            \begin{subfigure}{.75\columnwidth}
                \centering
                \includegraphics[width=\columnwidth]{Imgs/L2500Kn1s.png}
                \caption{$v_s=$ \SI{2500}{m/s} (Ma=7.73), $\Kn=1$}
                \label{fig:LDC_M7}
            \end{subfigure}
            \caption{Temperature and heat-flux for high Mach numbers.}
            \label{fig:LDC_highMach}
        \end{figure}
        
    \subsection{2D-Flow over Box}
    In analogy to the flow of argon over a flat-nose cylinder from Bird \cite{bird}, the two-dimensional flow over a square box with length $\ell = 2 \si{\centi \meter}$ was simulated. The gas has a number density of $10^{15} \si{\centi\meter}^{-3}$ and a velocity of $1000 \si{\meter \per \second}$, which corresponds to a Mach number of $5.37$ \cite{bird}. The top surface is at a temperature of $300 \si{\kelvin}$ and the side facing the flow was set to $1000 \si{\kelvin}$ (adiabatic case). A $80 \times 60$ grid was used with a cell size of $0.05 \si{\centi \meter}$. Taking $\ell$ as the characteristic length of the problem yields a Knudsen number of $\Kn = 0.11$. The density distribution and the temperature profile are presented in figure \ref{fig:adiabatic} as the ratio to the free stream density $\rho_\infty$ and the free stream temperature $T_\infty$ respectively.
    
    \begin{figure}
    \centering
    \begin{subfigure}{\columnwidth}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Adiabatic_Cylinder_density.png}
        \caption{Density distribution}
        \label{fig:adiabatic_density}
    \end{subfigure}
    \begin{subfigure}{\columnwidth}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Adiabatic_Cylinder_temperature.png}
        \caption{Temperature profile}
        \label{fig:adiabatic_temperature}
    \end{subfigure}
    \caption{Adiabatic-surface case with surface at $1000$K.}
    \label{fig:adiabatic}
    \end{figure}

    The simulation was repeated for the case of a cold surface at the side, which was set to $T = 300$K. The resulting density and temperature distribution are shown in figure \ref{fig:cold}.
    
    \begin{figure}
    \centering
    \begin{subfigure}{\columnwidth}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Cold_Cylinder_density.png}
        \caption{Density distribution}
        \label{fig:cold_density}
    \end{subfigure}
    \begin{subfigure}{\columnwidth}
        \centering
        \includegraphics[width=0.8\columnwidth]{Imgs/Cold_Cylinder_temperature.png}
        \caption{Temperature profile}
        \label{fig:cold_temperature}
    \end{subfigure}
    \caption{Cold-surface case with surface at $300$K.}
    \label{fig:cold}
    \end{figure}
    

\section{Discussion}
    \subsection{Implementation}
        The simulation works and is mostly parallelized. Improvements could be made by parallelising P2C, using a different method of binning particles. Another major improvement could be achieved by only checking particles in bounding cells for boundary conditions. For highly non-uniform systems load-balancing could improve performance further.

    \subsection{Test cases}
        The bimodal relaxation was the first simulation used to test the implementation of the collision and the periodic and thermal boundary conditions. The plots presented in the previous section show the expected results from the simulation, indicating that the implementation is working correctly.
    
        The Couvette flow test case demonstrates correspondence with the experiments detailed in Bird \cite{bird}. Our observed shear stress values fall within the range of the literature value
        \begin{equation*}
            \tau_{xy,\text{ lit.}} = \SI{6.35e-3}{\newton\per\meter^2}.
        \end{equation*}
        The gradient of the velocity profile is slightly too low compared to Bird due to much higher uncertainties. These higher uncertainties can be explained by the fact that, due to computational cost, we had to choose a higher number of microparticles per particle to obtain the same initial conditions as in \cite{bird}.
        
        The lid-driven cavity showed the violation of Fouriers law with the corresponding changes in heat-flux for high Mach numbers and matches the reference simulations in \cite{TopCavity} well. The only difference is that our simulation overall seems to be about \SI{5}{K} warmer. This could be caused by a smaller sample size and a different approach to binning the final data.
        
        The density distribution for the flow over the square box agrees well with the results from the similar simulation from Bird. However, the temperature profile differs from the flat-nose cylinder simulation. In the adiabiatic surface case, the higher temperature of the side compared to the top of the box is clearly reflected in the the temperature distribution in the gas. 
        This difference is absent in the cold surface case, which results in an evenly distributed temperature in the gas. The slight variations are most likely variations and uncertainties in the data.



\section{Conclusion}

In summary, the implemented DSMC simulation method shows promise in accurately capturing the behavior of rarefied gases, as demonstrated through various test cases. The Couvette flow test case aligns well with experimental results, with shear stress values falling within the range reported in the literature. The lid-driven cavity simulations exhibit non-equilibrium effects, showcasing violations of Fourier's law and changes in heat flux with increasing Mach numbers, consistent with reference simulations. Additionally, the simulation of flow over a box demonstrates agreement with expected density profiles for both adiabatic and cold surface cases.

However, there is still room for improvement in the implementation, particularly in optimizing computational efficiency by parallelizing and therefore reducing uncertainties. Overall, the DSMC algorithm proves to be a valuable tool for studying rarefied gas dynamics.
        

%---------------------------------------------------------
\printbibliography
%---------------------------------------------------------

\end{document}