#include "Ippl.h"
#include "GasMan.h"

int main(int argc, char* argv[]) {
    ippl::initialize(argc, argv);
    {
        Inform msg(TestName);

        msg << "Hello World" << endl;
    }
    ippl::finalize();

    return 0;
}

