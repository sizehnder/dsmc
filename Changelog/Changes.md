# Changelog

## Improved BC (not in final)

The boundary conditions were sped up by only checking for BC on particles which are in a boundary cell. This gave a speed-up of ~3-5x for the BC subroutine (depending on the problem).

Since the boundary conditions make up less than 10% of the runtime and this implementation would require entirely rewritting all testcases it was **not** added to the main branch. On the example of the Lid-driven cavity there was an overall speed-up of ~2%.

Additionaly it made the simulation unstable in the sense that if a particle velocity was unfortunately sampled (high speed with no collision) it was possible to skip through the boundary cells and thus leave the domain, crashing the simulation as a result.

## Implemented continous moment sampling

Allows to quickly sample the final state of the simulation. `momInteg1D/2D` can calculate the desired moments ("temp","hFlux","dens","vel") from the current gas state and write it to a CSV.

After the simulation has reached a stable state we can sample several 1000 timesteps which allows us to refrain from using smoothing on the final plots.

## Removed $N^2$ complexity of $g_{max}$

Originally the maximum relative velocity was calculated each step for every cell by comparing all the particles in the cell. This had $\mathcal{O}(N^2)$ complexity.

Per the suggestion from Mohsen this was changed such that $g_{max}$ is only changed if one of the particles colliding show a higher relative velocity. An initial guess of $g_{max}$ needs to given with
$$g_{max} = 3\cdot\max(\sqrt{k_BT/m},U_w)$$
The overall speed-up (Lid-driven cavity) was ~2.5x.

## Correcting resolution

As pointed out the timestep $\Delta t$ was changed to resolve both cell traversal and mean free path $\lambda$. This was achieved with
$$\Delta t = \frac{1}{2} \frac{\min(cellSize, \lambda)}{\max(k_BT/m,U_w)}$$
#### Checklist:
- [ ] Bimodal flow
- [ ] Couette flow
- [ ] Fourier flow
- [X] Lid-driven cavity
- [ ] Flow over box
- [ ] ...

## Changed Argon cross-section $\sigma$
Argon cross-section at 300K (0.02eV) is set to $7\cdot10^{-17}$ m² according to Fig.5 in this [this paper](https://iopscience.iop.org/article/10.1088/0953-4075/33/16/303/pdf). This will allow for direct comparison to real-life where applicable. In some cases the cross-section is given by the test-case.
#### Checklist:
- [ ] Bimodal flow
- [ ] Couette flow
- [ ] Fourier flow
- [X] Lid-driven cavity
- [ ] Flow over box
- [ ] ...


## Refractoring (switched to TestRange)
The first action was to remove the useless header file and move everything into `GasMan.h` (GasManager). Technically the other version is correcter and should decrease compile times, however since I have no idea how to set up the MakeFile to make use of that, it still has to compile everything multiple times. It means we do not have to update `RarefiedGas.h` when there is a change to `RarefiedGas.cpp`.

Along the way I removed some useless/duplicate functions (i.e `generateInitial` as this is different for every case anyway). In general the idea here is to make `GasMan.h` as multipurpose as possible and any fancy, complex extra cases can be implemented in the corresponding test case script itself.

The existing test cases are altered as needed.

## Radial symmetric problems
Messed around with radial symmetric problems which means that the cell volume for N_cand needs to be scales according to the radius position of the cell. Anyways, that is implemented in `p2cRadial`. The outer cells will have a volume factor $f$ of 1 that decreases towards the center following this formula I derived

$$f = \frac{(y_i+1)^2 - y_i^2}{y_{max}^2 - (y_{max}-1)^2}$$

where $y_i$ is the y-coordinate of the cell and $y_{max}$ the total number of cells along the y-axis.

To check it I tried to design a cone which needed a fancy custom BC (see [testCone](../testCone.cpp)). **The problem with partial cells is that technically the volume is smaller which needs to be addressed at some point**. The problem has a shearing lid with a thermal cone at 300K, all other walls are specular for now. At first i tried to make left/right periodic but then it would randomly grind to a halt because particles were actively moved into the cone.

For now I can't really tell the difference between `p2c2D` and `p2cRadial`. That is probably due to the fact that currently it is a glorified Lid-driven Cavity since we are lacking inflow/outflow boundaries.

![test](Imgs/p2c2D.png)
![test](Imgs/p2cRadial.png)


## Inflow/Outflow
After some fighting I got the Outflow working, it is called `BC_voiding`. It will delete particles that go behind the boundary.

The problem is, you will (almost) never encounter a case that is venting into an infinte vacuum. Thus it only makes sense together with an inflow. For now we can empty into vacuum which looks like this:

![test](Imgs/Void.png)

This was done as a radial problem with the right wall being vacuum. Note that after a certain amount of steps, usually when about 90% has drained the voiding condition will just grind to a halt for whatever reason, it doesn't crash, just doesn't continue (goes single core). The problem with inflow is that when I create particles I have to give them their attributes which doesn't work (or I don't know how, which is more likely).

Okay this is me a day later, I have figured out that creating new particles voids the attributes off all others. So now I just make a copy of them before creating new ones and you get this:

![test](Imgs/Flow.png)

Awesome! One has to play around a bit with the inflow condition but the simulation stabilises at a certain particle number. `BC_inflow` takes an area where you want to create your "ghost particles". I would recommend to just use like a line of cells along the boundary, then you can set a temperature and a number density of the "ghost gas". With these parameters you can control the flux and speed.

It is **important** that you do the inflow first, then do all your other boundary stuff and then lastly use `BC_void` to get rid of the "ghosts" that haven't made it into the domain. If you don't do this, the simulation will crash because there are particles outside of the domain.

At first I wanted to only create the particles that actually make it into the domain, but that was too much of a hassle, especially since I didn't know about the actual problem back then. Note the crashing "feature" can be disabled by uncommenting the if statement in `p2cRadial`, it is not advised though.

For real "outflow" the best way is to make a very teeny tiny amount of inflow and void as usual, it is more realistic than emptying into a infinite vacuum.

## Partial cells

When first introducing the cone boundary I mentioned that the boundary cells will not act correctly. This is because particles only can take a partial amount of these cells. Thus the number density will be off. This is now remedeed.

Note since there is no reason to have partial boundaries in the 1D case this will only be implemented for 2D.

This is implemented in 2 changes: First the function `cellWeights2D` was added. It takes in a reduced form of boundary conditions as a function and a number of samples. By reduced BC I mean that inflow/outflow is prohibited, if need be it can be replaced by a specular boundary. Additionally any BC which do not split cells in half do not need to be present. The number of samples determines the accuracy of the volume estimation.

`cellWeights2D` works by creating uniformly distributed particles in each cell and applies BC to them to see which particles where pushed by BC. The ratio of non-moved to total particles determines the volume. This function return a vector of percentages which can be given to P2C. It only needs to be called once before starting the integration.

The second change is to both `p2c2D` and `p2cRadial`. They can now take in a second argument, the pointer to the data of the vector produced by `cellWeights2D`. E.g `p2cRadial(gas,weights.data())` with weights being the result of `cellWeights2D`. If no weights are given they just assume all weights as 1 an thus this change (hopefully) is completely backwards compatible.

A minor fix was needed since `collideInCell` didn't check if the volume is 0 which resulted in a div0.

Regardless with this implemented and working (I checked), the actual change in the cone test case is negligible:

![test](Imgs/noWeights.png)
![test](Imgs/withWeights.png)

And no, the partial shading on the cone boundary is a result of the plot not the actual simulation :)

# Some testing

I decided to do some testing with a more "extreme" case by increasing the flow to make all the changes above more visible.

![test](Imgs/Kn1.png)
![test](Imgs/Kn0_1.png)

Kn 1 is the general case, the only notable thing here is, that the spectral wall (outer wall aka top) is a bit to close, so the shock front is not as nice. With Kn 0.1 this is less of an issue.

![test](Imgs/noRadial.png)
![test](Imgs/noPartialCell.png)

The change between `p2c2D` and `p2cRadial` is much more visible here. However the changes of partial cells are still not really noticeable.

---

# Nothing new

I think with that I have implemented all the changes that I wanted. Of course there are still many possible improvements such as
- 3D
- gas mixtures
- tree optimizations
- more flexible BC

and many more, however I am done. Bye.


