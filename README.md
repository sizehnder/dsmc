# DSMC for Rarefied Gas

Welcome to this repo! To get you started look at:
- [IPPL Installation Guide](#ippl-installation)
- [Testing IPPL](#testing-ippl)
- [Adding DSMC](#get-dsmc)

For a theoretical background and results look at:
- [Final report](Presentations/Project_Report.pdf)
- [Theory presentation](Presentations/CSP_Theory_Presentation.pdf)
- [Final presentation](Presentations/CSP_EndPresentation.pdf)

An informal overview of what has been done after the final presentation can be seen in the [changelog](Changelog/Changes.md).

---
---

### IPPL installation

The two [IPPL GitHub](https://github.com/IPPL-framework/ippl) repositories are cloned into your working folder with
```
git clone https://github.com/IPPL-framework/ippl.git
git clone https://github.com/IPPL-framework/ippl-build-scripts.git
```
following the guide for installing on [Euler](https://amas.web.psi.ch/people/aadelmann/pub/ippl-doc/doc/html/Installation.html). It seems that the current version of IPPL is bugged and therefore needs to be reverted to a stable release:

```
cd ippl/
git checkout tags/IPPL-3.2.0
cd ..
```

Install IPPL with the build_scripts as follows
```
./ippl-build-scripts/999-build-everything -t serial -k -f -i -u
```
maybe you'll get an error along the lines of "MPI-CXX not fount", then you just need to install [OPENMPI](https://brandonrozek.com/blog/openmpi-fedora). Be sure to load the module with
```
module load mpi/openmpi-x86_64
```
For multi-threading use `openmd` instead of `serial` (note that the build-folder will then change name from `build_serial` to `build_openmd`).

Note: For some reason the current instructions are unable to build the `openmd` build (no clue why, 1 month ago it worked).

---

### Testing IPPL

You can test out the install by doing

```
cd ippl/build_serial/alpine/
make
./PenningTrap 128 128 128 10000 20 FFT 0.01 LeapFrog --overallocate 1.0 --info 10
```

where 20 is the number of steps.

---

### Get DSMC

Inside the `/ippl` folder load the DSMC repo with

```
git clone https://gitlab.ethz.ch/sizehnder/dsmc.git
```

in the file `/ippl/CMakeLists.txt` add the line

```
add_subdirectory(dsmc)
```

as line 101. After that use

```
cd build_serial/
cmake ..
```

To create a folder called `dsmc` inside `build_serial` where the executable and the Makefile can be found. To make the executabels use

```
cd dsmc/
make
```

The output will show you all the executable that are made (e.g -- Built target HelloWorld). The executables can the be run using

```
./HelloWorld --info 10
```

(or however your executable is called). If you add a new .cpp file, make sure to update `/ippl/dsmc/CMakeLists.txt` by adding the lines

```
add_executable (ExecName GasMan.h ScriptName.cpp)
target_link_libraries (ExecName ${IPPL_LIBS})
```

replacing "ExecName" with the name you want to give to the executable and "ScripName.cpp" with your .cpp file. `GasMan.h` is our manager for the DSMC functionality, it needs to be included if it is used in your script.

---


