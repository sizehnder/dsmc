#include "Ippl.h"
#include "Random/Randn.h"
#include <random>
#include <vector>

#include "GasMan.h"

// #time steps | #particles | gridX
// ./Shear 100000 10000 32 --info 10 (0.01s)
// ./Shear 10 3 4 --info 10
Vector<double,3> domainSize;
double lidSpeed = 1.0;
double T_norm = 273.0;
const double k = 1.38e-23; // (m^2 kg / s^2 K)
double m = 6.6e-26; // Argon (kg)
std::mt19937 RNG_1(420);


Particles generateInitial(const size_t numP, Vector<double,Dim> domainSize){
    Particles gas(new Particles::Layout_t());
    gas.create(numP);
    
    std::uniform_real_distribution<double> uDist(0.0,1.0);
    
    for (size_t i = 0; i < numP; i++){
        gas.M(i) = m;
        gas.R(i) = Vector<double,Dim>(uDist(RNG_1), uDist(RNG_1), 0) * domainSize;  // Random initial positions (normal dist)
        gas.V(i) = (Vector<double,Dim>(uDist(RNG_1), uDist(RNG_1), 0) - Vector<double,Dim>(0.5,0.5,0)) * lidSpeed;
    }
    gas.update();
    return gas;
}

void integrate(Particles& gas, unsigned int steps, bool talk = true){
    for (unsigned int t = 0; t < steps; t++){
        if (talk && (t*10) % steps == 0){
            msg << t << "/" << steps << endl;
        }
        p2c1D(gas);
        moveParticles(gas);
        BC_thermal(gas,0,0.0,false,k*T_norm,0.0,1);                     //left
        BC_thermal(gas,0,domainSize[0],true,k*T_norm,lidSpeed,1);            //right

        gas.update();
    }
}


int main(int argc, char* argv[]) {

    initialize(argc, argv);
    {
        msg << "---==={ Couette flow }===---" << endl;
        // #steps | n | #cells(axis) | p/cell | v_lid | Kn
        // ./Couette 1000 1e20 100 80 300 1 --info 10

        // handle input
        const uint nt = std::atoi(argv[1]); // Number of time steps (3000 init, +samples)
        double avgNumDens = std::atof(argv[2]); // Number density (10^21 / m^3)
        cellLayout = {std::atoi(argv[3]),0,0}; // gridLayout (150x150)
        const uint macrosPerCell = std::atoi(argv[4]); // Avg number of macroparticles per (500)
        lidSpeed = std::atof(argv[5]); // Lid speed (100, 300 m/s)
        const double Kn = std::atof(argv[6]); // Knudsen number [0.01,10]

        sig_T = 1e-20; // Argon (~1eV)
        double meanFreePath = 1 / (avgNumDens * sig_T);
        double L = meanFreePath / Kn;
        domainSize = {L,0,0};
        cellSize = L / cellLayout[0];
        size_t nP = macrosPerCell * cellLayout[0];
        micPmac = avgNumDens / (domainSize[0]) / nP;


        dT = 0.5 * std::min(cellSize,meanFreePath) / std::max(sqrt(k*T_norm / m),lidSpeed);
        msg << "Time steps: " << nt << "   (dt=" << dT << ")" << endl;
        msg << "Grid: " << cellLayout << "   (cSize=" << cellSize << ")" << endl;
        msg << "DomainSize: " << domainSize << endl;
        msg << "No of (macro)particles: " << nP << "   (mic/mac=" << micPmac << ")" << endl;
        msg << "Temperature: " << T_norm << endl;
        msg << "Lid velocity: " << lidSpeed << endl;
        msg << "---==========================---" << endl;
        Particles gas = generateInitial(nP, domainSize);
        //histo(gas,20,"pos",0);
        //histo(gas,20,"pos",1);
        histo(gas,20,"vel",0);
        // histo(gas,20,"vel", 1);

        msg << "integrating..." << endl;
        integrate(gas, nt);
        msg << "...done" << endl;

        //histo(gas,20,"pos",0);
        //histo(gas,20,"pos",1);
        histo(gas,20,"vel",0);
        csvExport(gas,"Couette.csv");
    }
    finalize();

    return 0;
}
