const char* TestName = "IPPL";

#include "Ippl.h"
#include "Random/Randn.h" // work with random::randn<double,Dim>(viewType,randPool)
#include <random>
#include <vector>

using namespace ippl;



// Seed the random number generator
//std::random_device rd; // for random seed
std::mt19937 RNG(420);

Inform msg(TestName);
const unsigned Dim = 3;
const double k_B = 1.38e-23; // (m^2 kg / s^2 K)
Vector<int,Dim> cellLayout;
double cellSize;
double dT;
double nDens;
double sig_T;
double micPmac;
double g_max;

// ---=== CLASSES ===---

/**
 * Paricle class with attributes id, pos, velocity, mass (ID,R,V,M)
 * 
 * @param L ParticleLayout (IPPL)
 */
class Particles : public ParticleBase<ParticleSpatialLayout<double,Dim>> {
public:
    // Attributes for this particle class (besides position (R) and ID).
    ParticleAttrib<double> M;               // mass
    ParticleAttrib<Vector<double,Dim>> V;   // velocity (could also use ParticlePos_t instead of vector)
    // constructor
    Particles(Layout_t *L) : ParticleBase<Layout_t>(*L) {
        addAttribute(M);
        addAttribute(V);
    }
};


// ---=== HELPER FUNCTIONS ===---
// --- VECTORS ---
/**
 * Returns the Euclidean norm of a 3-vector
 * 
 * @param v 3d-vector (IPPL)
 * @return double (euclidian norm)
 */
double norm(const Vector<double,3>& v) {
    return std::sqrt(v[0]*v[0] + v[1]*v[1] + v[2]*v[2]);
}

/**
 * Returns random 3-Vector uniformly distributed on unit sphere
 * 
 * @return 3d-vector (IPPL), normalized
 */
Vector<double, 3> randomUSphere() {
    // Create a random number generator
    std::uniform_real_distribution<double> dis(0, 1); 

    // Generate random angles
    double theta = 2 * M_PI * dis(RNG);
    double phi = acos(1 - 2 * dis(RNG));

    return Vector<double, 3>(sin(phi) * cos(theta), sin(phi) * sin(theta), cos(phi));
}


// --- DATA SAVING ---
/**
 * Write (particle) data to a CSV file
 * 
 * @param gas Collection of Particles to be saved
 * @param filename Name of .csv file
 * @param n How many particles get saved (the first n)
 * @return void
 */
void csvExport(Particles& gas, std::string fileName, size_t n=0){
    // Create file
    std::ofstream file((fileName + ".csv"));
    if (n == 0 || n > gas.getTotalNum()){
        n = gas.getTotalNum();
    }

    // z-axis is never really needed
    file << "posX, posY, velX, velY, mass (size=" << n << ")\n";
    // Write data
    for (size_t i = 0; i < n; i++){
        file << gas.R(i)[0] << "," << gas.R(i)[1] << ",";
        file << gas.V(i)[0] << "," << gas.V(i)[1] << ",";
        file << gas.M(i) << "\n";
    }

    // Close file
    file.close();
}

/**
 * Write property distribution as a line into the saveFile
 * 
 * @param gas Collection of Particles to be saved
 * @param saveFile Open file to write to
 * @param division Number of bins
 * @param domainSize Size of domain
 * @param property Property to calculate ("temp","hFlux","dens","vel")
 * @return void
 */
void momInteg1D(Particles& gas, std::ofstream& saveFile, uint division, double domainSize, std::string property = "dens"){
    if (property == "temp"){
        Vector<double,2> meanVel[division];
        // Find mean velocity
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            meanVel[static_cast<int>(gas.R(i)[0]/domainSize*division)] += Vector<double,2>(gas.V(i)[0],1);
        }
        // relative velocity
        Vector<double,2> T[division];
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            int ind = static_cast<int>(gas.R(i)[0]/domainSize*division);
            double v_ = gas.V(i)[0] - (meanVel[ind][0] / meanVel[ind][1]);
            T[ind] += Vector<double,2>(v_*v_,1);
        }
        // Write result to saveFile
        for (uint n = 0; n < division-1; n++){
            saveFile << (T[n][0]*gas.M(0)/k_B / T[n][1]) << ",";
        }
        saveFile << (T[division-1][0]*gas.M(0)/k_B / T[division-1][1]) << "\n";
    }
    else if (property == "hFlux"){
        Vector<double,2> meanVel[division];
        // Find mean velocity
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            meanVel[static_cast<int>(gas.R(i)[0]/domainSize*division)] += Vector<double,2>(gas.V(i)[0],1);
        }
        // relative velocity
        Vector<double,2> q[division];
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            int ind = static_cast<int>(gas.R(i)[0]/domainSize*division);
            double v_ = gas.V(i)[0] - (meanVel[ind][0] / meanVel[ind][1]);
            q[ind] += Vector<double,2>(v_*v_*v_,1);
        }
        // Write result to saveFile
        for (uint n = 0; n < division-1; n++){
            saveFile << (q[n][0] / q[n][1]/2) << ",";
        }
        saveFile << (q[division-1][0] / q[division-1][1]/2) << "\n";
    }
    else if (property == "dens"){
        int N[division] = {0};
        // Find number of particles
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            N[static_cast<int>(gas.R(i)[0]/domainSize*division)]++;
        }
        // Write result to saveFile
        for (uint n = 0; n < division-1; n++){
            saveFile << N[n] << ",";
        }
        saveFile << N[division-1] << "\n";
    }
    else if (property == "vel"){
        Vector<double,2> vel[division];
        // Find mean velocity
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            vel[static_cast<int>(gas.R(i)[0]/domainSize*division)] += Vector<double,2>(gas.V(i)[0],1);
        }
        // Write result to saveFile
        for (uint n = 0; n < division-1; n++){
            saveFile << (vel[n][0]/vel[n][1]) << ",";
        }
        saveFile << (vel[division-1][0]/vel[division-1][1]) << "\n";
    }
    else{
        msg << "Property is not available: " << property << endl;
    }
    // If anyone else wants to implement pressure tensor and shearing stress, go for it :)
}

/**
 * Write property distribution as a line into the saveFile
 * 
 * @param gas Collection of Particles to be saved
 * @param saveFile Open file to write to
 * @param division Number of bins (2d)
 * @param domainSize Size of domain (2d)
 * @param property Property to calculate ("temp","hFlux","dens","vel")
 * @return void
 */
void momInteg2D(Particles& gas, std::ofstream& saveFile, Vector<int,2> division, Vector<double,2> domainSize, std::string property = "dens"){
    if (property == "temp"){
        std::vector<std::vector<Vector<double,3>>> vel(division[0], std::vector<Vector<double,3>>(division[1],Vector<double,3>(0,0,0))); // Vector filled with 0s
        // Find mean velocity
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            vel[static_cast<int>(gas.R(i)[0]/domainSize[0]*division[0])][static_cast<int>(gas.R(i)[1]/domainSize[1]*division[1])]
            += Vector<double,3>(gas.V(i)[0],gas.V(i)[1],1);
        }
        // relative velocity
        std::vector<std::vector<Vector<double,2>>> T(division[0], std::vector<Vector<double,2>>(division[1],Vector<double,2>(0,0))); // Vector filled with 0s
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            int x = static_cast<int>(gas.R(i)[0]/domainSize[0]*division[0]);
            int y = static_cast<int>(gas.R(i)[1]/domainSize[1]*division[1]);
            Vector<double,2> v_ = {gas.V(i)[0] - vel[x][y][0]/vel[x][y][2], gas.V(i)[1] - vel[x][y][1]/vel[x][y][2]};
            T[x][y] += Vector<double,2>(v_[0]*v_[0] + v_[1]*v_[1], 1);
        }

        // Write result to saveFile
        for (int x = 0; x < division[0]-1; x++){
            for (int y = 0; y < division[1]; y++){
                saveFile << T[x][y][0]/(T[x][y][1]*2*k_B)*gas.M(0) << ",";
            }
        }
        for (int y = 0; y < division[1]-1; y++){
            saveFile << T[division[0]-1][y][0]/(T[division[0]-1][y][1]*2*k_B)*gas.M(0) << ",";
        }
        saveFile << T[division[0]-1][division[1]-1][0]/(T[division[0]-1][division[1]-1][1]*2*k_B)*gas.M(0) << "\n";
    }
    else if (property == "hFlux"){
        std::vector<std::vector<Vector<double,3>>> vel(division[0], std::vector<Vector<double,3>>(division[1],Vector<double,3>(0,0,0))); // Vector filled with 0s
        // Find mean velocity
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            vel[static_cast<int>(gas.R(i)[0]/domainSize[0]*division[0])][static_cast<int>(gas.R(i)[1]/domainSize[1]*division[1])]
            += Vector<double,3>(gas.V(i)[0],gas.V(i)[1],1);
        }
        // relative velocity
        std::vector<std::vector<Vector<double,3>>> Q(division[0], std::vector<Vector<double,3>>(division[1],Vector<double,3>(0,0,0))); // Vector filled with 0s
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            int x = static_cast<int>(gas.R(i)[0]/domainSize[0]*division[0]);
            int y = static_cast<int>(gas.R(i)[1]/domainSize[1]*division[1]);
            Vector<double,2> v_ = {gas.V(i)[0] - vel[x][y][0]/vel[x][y][2], gas.V(i)[1] - vel[x][y][1]/vel[x][y][2]};
            double v2 = v_[0]*v_[0] + v_[1]*v_[1];
            Q[x][y] += Vector<double,3>(v_[0]*v2,v_[1]*v2, 1);
        }

        // Write result to saveFile (writes axis to separate lines)
        for (uint ax = 0; ax < 2; ax++){
            for (int x = 0; x < division[0]-1; x++){
                for (int y = 0; y < division[1]; y++){
                    saveFile << Q[x][y][ax]/Q[x][y][2]/2 << ",";
                }
            }
            for (int y = 0; y < division[1]-1; y++){
                saveFile << Q[division[0]-1][y][ax]/Q[division[0]-1][y][2]/2 << ",";
            }
            saveFile << Q[division[0]-1][division[1]-1][ax]/Q[division[0]-1][division[1]-1][2]/2 << "\n";
        }

    }
    else if (property == "dens"){
        std::vector<std::vector<int>> N(division[0], std::vector<int>(division[1],0)); // Vector filled with 0s
        // Find number of particles
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            N[static_cast<int>(gas.R(i)[0]/domainSize[0]*division[0])][static_cast<int>(gas.R(i)[1]/domainSize[1]*division[1])]++;
        }
        // Write result to saveFile
        for (int x = 0; x < division[0]-1; x++){
            for (int y = 0; y < division[1]; y++){
                saveFile << N[x][y] << ",";
            }
        }
        for (int y = 0; y < division[1]-1; y++){
            saveFile << N[division[0]-1][y] << ",";
        }
        saveFile << N[division[0]-1][division[1]-1] << "\n";
    }
    else if (property == "vel"){
        std::vector<std::vector<Vector<double,3>>> vel(division[0], std::vector<Vector<double,3>>(division[1],Vector<double,3>(0,0,0))); // Vector filled with 0s
        // Find mean velocity
        for (size_t i = 0; i < gas.getTotalNum(); i++){
            vel[static_cast<int>(gas.R(i)[0]/domainSize[0]*division[0])][static_cast<int>(gas.R(i)[1]/domainSize[1]*division[1])]
            += Vector<double,3>(gas.V(i)[0],gas.V(i)[1],1);
        }
        // Write result to saveFile (writes axis to separate lines)
        for (uint ax = 0; ax < 2; ax++){
            for (int x = 0; x < division[0]-1; x++){
                for (int y = 0; y < division[1]; y++){
                    saveFile << vel[x][y][ax]/vel[x][y][2] << ",";
                }
            }
            for (int y = 0; y < division[1]-1; y++){
                saveFile << vel[division[0]-1][y][ax]/vel[division[0]-1][y][2] << ",";
            }
            saveFile << vel[division[0]-1][division[1]-1][ax]/vel[division[0]-1][division[1]-1][2] << "\n";
        }
        
    }
    else{
        msg << "Property is not available: " << property << endl;
    }
    // If anyone else wants to implement pressure tensor and shearing stress, go for it :)
}


// --- PHYSICS ---
/**
 * Moves particles according to x' = x + v*dt
 * 
 * @param gas Collection of Particles
 * @return void
 */
void moveParticles(Particles& gas) {
    gas.R = gas.R + gas.V * dT;
}

/**
 * Performs hard sphere collision between particle i and j
 * 
 * @param gas Collection of Particles
 * @param i Index of particle 1
 * @param j Index of particle 2
 * @return gas after collision
 */
Particles hardSphereCollision(Particles& gas, size_t i, size_t j) {
    // Create a random number generator
    double m1 = gas.M(i);
    double m2 = gas.M(j);

    Vector<double, Dim> v1 = gas.V(i);
    Vector<double, Dim> v2 = gas.V(j);

    Vector<double, Dim> g = randomUSphere() * norm(v1 - v2); // Hard sphere scattering results in uniform distribution

    Vector<double, Dim> v_m = (m1 * v1 + m2 * v2) / (m1 + m2);

    gas.V(i) = v_m + (m2 / (m1 + m2)) * g;
    gas.V(j) = v_m - (m1 / (m1 + m2)) * g;

    return gas;
}


// --- DEBUG --- (also kinda useless by now, but maybe useful for debugging)
/**
 * Prints the parameters of the first n particles
 * 
 * @param gas Collection of Particles
 * @param n How many particles to print
 * @return void
 */
void printParticles(Particles& gas, size_t n=5){
    for (size_t i = 0; i < n; i++){
        msg << gas.R(i) << " | " << gas.V(i) << " | " << gas.M(i) << endl;
    }
    return;
}

/**
 * Plots an ASCII histogram into the console
 * 
 * @param gas Collection of Particles
 * @param bins Number of histogram bins
 * @param param Which parameter to plot ("vel","velAbs","pos")
 * @param dim Which parameter dimension to use [0,2]
 * @param norm Integrated value of histogram
 * @return void
 */
void histo(Particles& gas, int bins=20, std::string param="vel", uint dim=0, double norm=100){
    double count[bins];

    // Initialize all elements to zero
    for (int i = 0; i < bins; ++i) {
        count[i] = 0.0;
    }

    double w = norm / gas.getTotalNum();
    double mini = 0;
    double maxi = 0;
    

    // calculate binning
    if (param == "vel"){
        mini = gas.V(0)[dim];
        maxi = gas.V(0)[dim];
        std::cout << "Velocity (" << dim << ")" << std::endl;

        for (size_t i = 0; i < gas.getTotalNum();i++){
            if (mini > gas.V(i)[dim]){
                mini = gas.V(i)[dim];
            }
            else if (maxi < gas.V(i)[dim]){
                maxi = gas.V(i)[dim];
            }
        }
        
        for (size_t i = 0; i < gas.getTotalNum();i++){
            double val = (gas.V(i)[dim] - mini)/(maxi - mini);
            count[static_cast<int>(val * bins)] += w;
        }
    }
    else if(param == "velAbs"){
        mini = 0.0;
        maxi = 0.0;
        std::cout << "absVelocity (" << dim << ")" << std::endl;

        for (size_t i = 0; i < gas.getTotalNum();i++){
            if (maxi < gas.V(i)[dim]){
                maxi = gas.V(i)[dim];
            }
        }
        
        for (size_t i = 0; i < gas.getTotalNum();i++){
            double val = (abs(gas.V(i)[dim]) - mini)/(maxi - mini);
            count[static_cast<int>(val * bins)] += w;
        }
    }
    else if (param == "pos"){
        mini = gas.R(0)[dim];
        maxi = gas.R(0)[dim];
        std::cout << "Position (" << dim << ")" << std::endl;

        for (size_t i = 0; i < gas.getTotalNum();i++){
            if (mini > gas.R(i)[dim]){
                mini = gas.R(i)[dim];
            }
            else if (maxi < gas.R(i)[dim]){
                maxi = gas.R(i)[dim];
            }
        }
        
        for (size_t i = 0; i < gas.getTotalNum();i++){
            double val = (gas.R(i)[dim] - mini)/(maxi - mini);
            count[static_cast<int>(val * bins)] += w;
        }
    }
    else{
        msg << "[WARNING] Histogramm parameter not implemented" << endl;
        return;
    }
    
    // display
    std::cout << "min: " << mini << " | max: " << maxi << std::endl;
    std::cout << "+------------------------->" << std::endl;
    for (int x = 0; x < bins; x++){
        std::cout << "| ";
        for (int i = 0; i < static_cast<int>(count[x]); i++){
            std::cout << "X";
        }
        std::cout << "              " << count[x]/w << std::endl;
    }
    std::cout << "V" << std::endl;
    return;
}


// ---=== MAIN FUNCTIONS ===---
/**
 * Perform DSMC collision inside a cell
 * 
 * @param gas Collection of Particles
 * @param indx Indices of all particles in that cell
 * @param cVol Cell volume
 * @return void
 */
void collideInCell(Particles& gas, std::vector<size_t> indx, double cVol){
    size_t N_cell = indx.size();
    if (cVol == 0 || N_cell == 0){return;}
    std::uniform_int_distribution<size_t> uIntDist(0,N_cell-1);
    double loc_Dens = N_cell * micPmac / cVol;
    double N_cand = loc_Dens * N_cell * sig_T * g_max * dT;
    
    std::uniform_real_distribution<double> uDist(0.0,1.0);
    if (N_cand < uDist(RNG)){return;} // handle N_cand < 1
    // from here on N_cand = 1
    for (size_t i = 0; i < N_cand; i++){
        size_t i1 = 0;
        size_t i2 = 0;

        do{ // choose two particles to collide
            i1 = indx[uIntDist(RNG)];
            i2 = indx[uIntDist(RNG)];
        } while (i1 == i2);

        double g = norm(gas.V(i1) - gas.V(i2));
        g_max = std::max(g,g_max); // Update g_max if needed
        double r = uDist(RNG);

        if (g/g_max > r){
            hardSphereCollision(gas, i1, i2);
        }
    }
    return;
}

/**
 * Bunches particles according to which cell they reside in (1D) and perform the DSMC collision for each cell
 * 
 * @param gas Collection of Particles
 * @return void
 */
void p2c1D(Particles& gas){
    std::vector<size_t> distrib1D[cellLayout[0]];
    std::vector<size_t>* pD = &distrib1D[0];
    Particles* pc = &gas;

    // can't parallelize push_back
    for (size_t i = 0; i < gas.getTotalNum(); i++){
        int x = static_cast<int>(gas.R(i)[0]/cellSize);
        distrib1D[x].push_back(i);
    }

    Kokkos::parallel_for("Coll_1D", cellLayout[0], KOKKOS_LAMBDA(const int x){
        collideInCell(*pc, *(pD + x),cellSize);
    });
}

/**
 * Bunches particles according to which cell they reside in (2D) and perform the DSMC collision for each cell
 * 
 * @param gas Collection of Particles
 * @param weights Ptr to vector<double> with the same size as domain containing cell weights
 * @return void
 */
void p2c2D(Particles& gas, double *weigths = nullptr){
    std::vector<size_t> distrib2D[cellLayout[0]][cellLayout[1]];
    std::vector<size_t>* pD = &distrib2D[0][0];
    Particles* pc = &gas;

    // can't parallelize push_back
    for (size_t i = 0; i < gas.getTotalNum(); i++){
        int x = static_cast<int>(gas.R(i)[0]/cellSize); int y = static_cast<int>(gas.R(i)[1]/cellSize);
        distrib2D[x][y].push_back(i);
    }

    
    if (weigths){
        Kokkos::parallel_for("Coll_2D", cellLayout[0]*cellLayout[1], KOKKOS_LAMBDA(const int xy){ 
            collideInCell(*pc, *(pD + xy),cellSize*cellSize*(*(weigths+xy)));
        });
    }
    else{
        Kokkos::parallel_for("Coll_2D", cellLayout[0]*cellLayout[1], KOKKOS_LAMBDA(const int xy){ 
            collideInCell(*pc, *(pD + xy),cellSize*cellSize);
        });
    }
}

/**
 * Bunches particles according to which cell they reside in (2D) and perform the DSMC collision for each cell.
 * Used for axial symmetric problems, applies extra factor depending on distance from symmetry axis (always y).
 * 
 * @param gas Collection of Particles
 * @param weights Ptr to vector<double> with the same size as domain containing cell weights
 * @return void
 */
void p2cRadial(Particles& gas, double *weigths = nullptr){
    std::vector<size_t> distrib2D[cellLayout[0]][cellLayout[1]];
    std::vector<size_t>* pD = &distrib2D[0][0];
    Particles* pc = &gas;

    // can't parallelize push_back
    for (size_t i = 0; i < gas.getTotalNum(); i++){
        int x = static_cast<int>(gas.R(i)[0]/cellSize); int y = static_cast<int>(gas.R(i)[1]/cellSize);
        //if (x<0 || y<0 || x > cellLayout[0] || y > cellLayout[1]){msg << x << "," << y << endl;continue;}
        distrib2D[x][y].push_back(i);
    }

    if (weigths){
        Kokkos::parallel_for("Coll_rad2D", cellLayout[0]*cellLayout[1], KOKKOS_LAMBDA(const int xy){
            int r = xy % cellLayout[0]; // r = y-Pos
            double radialFactor = (pow(r+1,2) - pow(r,2)) / (pow(cellLayout[1],2) - pow(cellLayout[1]-1,2)); // value in ]0,1], outer cell is 1
            collideInCell(*pc, *(pD + xy),cellSize*cellSize*radialFactor*(*(weigths+xy)));
        });
    }
    else{
        Kokkos::parallel_for("Coll_rad2D", cellLayout[0]*cellLayout[1], KOKKOS_LAMBDA(const int xy){
            int r = xy % cellLayout[0]; // r = y-Pos
            double radialFactor = (pow(r+1,2) - pow(r,2)) / (pow(cellLayout[1],2) - pow(cellLayout[1]-1,2)); // value in ]0,1], outer cell is 1
            collideInCell(*pc, *(pD + xy),cellSize*cellSize*radialFactor);
        });
    }
    
}

/**
 * Determines the effective volume (percentage of actual volume) for all cells by creating sample particles and
 * applying the BC to them. It's a stupid but convenient way to do this :)
 * 
 * @param BC Function you defined the boundary conditions in, note voiding/inflow will not work so maybe needs a reduced BC function
 * @param samples Number of samples calculated per axis per cell
 * @return Vector of cell weights which can be given to P2C
 */
std::vector<double> cellWeights2D(void (*BC)(Particles&), size_t samples = 10){
    Particles sampleGas(new Particles::Layout_t());
    size_t totalSamps = samples*samples;
    sampleGas.create(totalSamps);

    double sampSpacing = cellSize / samples;
    std::vector<double> weights; // vector to store cellWeights in

    std::vector<Vector<double,3>> pPos; // calculate sampling positions
    for (size_t i = 0; i < samples; i++){
        for (size_t j = 0; j < samples; j++){
            pPos.push_back(Vector<double,3>(i+0.5,j+0.5,0)*sampSpacing);
        }
    }
    
    for (int x = 0; x < cellLayout[0]; x++){
        for (int y = 0; y < cellLayout[1]; y++){ // Check every cell seperately
            Vector<double,3> cellOrigin = {x*cellSize,y*cellSize,0};
            size_t unmoved = 0;

            // set sample particle positions
            for (size_t i = 0; i < totalSamps; i++){
                sampleGas.R(i) = cellOrigin + pPos[i];
            }
            BC(sampleGas);
            // check which ones moved
            for (size_t i = 0; i < totalSamps; i++){
                Vector<double,3> a = cellOrigin + pPos[i]; // this does not work inline, so here it is
                if (sampleGas.R(i)[0] == a[0] && sampleGas.R(i)[1] == a[1]){
                    unmoved++;
                }
            }
            // save the percentage of particles not affected by BC ~ volume of cell
            weights.push_back((double)(unmoved)/totalSamps);
        }
    }
    
    return weights;
}

// ---=== BOUNDARY CONDITIONS ===---
/**
 * Applies periodic boundary conditions on particles
 * 
 * @param gas Collection of Particles
 * @param dim In which dimension the boundary conditions apply [0-2]
 * @param bounds Position of the left and right boundary (2-Vector)
 * @return void
 */
void BC_periodic(Particles& gas, uint dim, Vector<double, 2> bounds){
    double box_size = bounds[1] - bounds[0];
    Kokkos::parallel_for("periodicBC", gas.getTotalNum(), KOKKOS_LAMBDA(const size_t i){
        gas.R(i)[dim] += box_size * ((gas.R(i)[dim] < bounds[0]) - (gas.R(i)[dim] > bounds[1]));
    });
    return;
}

/**
 * Applies specular boundary conditions on particles (mirrorlike reflection)
 * 
 * @param gas Collection of Particles
 * @param dim In which dimension the boundary conditions apply [0-2]
 * @param boundPos Position of boundary
 * @param positive If "wall" is on the positive direction of boundPos (wall normal negative)
 * @return void
 */
void BC_specular(Particles& gas, uint dim, double boundPos, bool positive){
    
    Kokkos::parallel_for("specularBC", gas.getTotalNum(), KOKKOS_LAMBDA(const size_t i){
        bool inWall = abs((gas.R(i)[dim] < boundPos) - positive); // 0 on domain, 1 outside
        gas.R(i)[dim] += 2 * (boundPos-gas.R(i)[dim])*inWall; // reflect if in wall
        gas.V(i)[dim] *= (1-2*inWall); // invert if in wall
    });
    return;
}

/**
 * Applies thermal boundary conditions on particles (with option for shearing)
 * 
 * @param gas Collection of Particles
 * @param dim In which dimension the boundary conditions apply [0-2]
 * @param boundPos Position of boundary
 * @param positive If "wall" is on the positive direction of boundPos (wall normal negative)
 * @param kT Constant k_B*T
 * @param v Shearing velocity
 * @param vDim Dimension in which the shearing movement is performed
 * @return void
 */
void BC_thermal(Particles& gas, uint dim, double boundPos, bool positive, double kT, double v, int vDim){
        
    Kokkos::parallel_for("thermalBC", gas.getTotalNum(), KOKKOS_LAMBDA(const size_t i){
        int dirToGo = (gas.R(i)[dim] < boundPos) - positive; // 1| 0 |-1
        if (abs(dirToGo)){ // if in boundary
            std::uniform_real_distribution<double> uDist(0.0,1.0); // needs to be here, maybe replace later with IPPL RNG
            std::normal_distribution<double> nDist(v,sqrt(kT/gas.M(i)));
            gas.R(i)[dim] += 2*(boundPos-gas.R(i)[dim]);

            double v_new = sqrt(-2*kT*log(uDist(RNG))/gas.M(i)); // resampling function from slides
            gas.V(i)[dim] = dirToGo*v_new; // invert and resample
            gas.V(i)[vDim] = nDist(RNG); // sample perp velocity
        }
        
    });
    
    return;
}

/**
 * Void boundary, deletes particles outside the bound
 * 
 * @param gas Collection of Particles
 * @param dim In which dimension the particles need to enter [0-2]
 * @param boundPos Wall position
 * @param positive If "wall" is on the positive direction of boundPos (wall normal negative)
 * @return void
 */
void BC_voiding(Particles& gas, uint dim, double boundPos, bool positive){
    size_t nOut = 0;
    Kokkos::View<bool*> killList("kill",gas.getTotalNum()); // race-free array (bool with length totalNum)
    if (positive){
        Kokkos::parallel_reduce("voidBC+", gas.getTotalNum(), KOKKOS_LAMBDA(const size_t i, size_t& killCount){
            killCount += (gas.R(i)[dim] >= boundPos);
            killList(i) = (gas.R(i)[dim] >= boundPos);
        },nOut);
        gas.destroy(killList,nOut);
    }
    else
    {
        Kokkos::parallel_reduce("voidBC-", gas.getTotalNum(), KOKKOS_LAMBDA(const size_t i, size_t& killCount){
            killCount += (gas.R(i)[dim] <= boundPos);
            killList(i) = (gas.R(i)[dim] <= boundPos);
        },nOut);
        gas.destroy(killList,nOut);
    }
    //msg << " " << nOut << endl;
}

/**
 * Inflow boundary (followed by BC_voiding afterwards). Currently only supports 1D/2D not 3D, because of area.
 * This boundary creates particles in the given area (with density and temperature) and creates all particles that land in domain.
 * 
 * @param gas Collection of Particles
 * @param area Ghost particle area (x1,x2,y1,y2)
 * @param kT Constant k_B*T, determines particle speed
 * @param n Particle density, determines particle flux
 * @return void
 */
void BC_inflow(Particles& gas, Vector<double,4> area, double kT, double n, double pMass){
    size_t nP = n / micPmac *((area[1]-area[0])*(area[3]-area[2]));
    size_t initCount = gas.getTotalNum();

    auto Rcopy = gas.R.getView();
    auto Vcopy = gas.V.getView();
    
    gas.create(nP); // For some bizarre reason this sets all attributes to 0 (even for existing ones)
    //msg << nP << endl;

    Kokkos::parallel_for("inflowCopy", initCount, KOKKOS_LAMBDA(const size_t i){
        gas.R(i) = Rcopy(i);
        gas.V(i) = Vcopy(i); // There is probably a smarter way to do this but idk
        gas.M(i) = pMass;
    });

    Kokkos::parallel_for("inflowBC", nP, KOKKOS_LAMBDA(const size_t i){
        std::uniform_real_distribution<double> uDist(0.0,1.0);
        // not sure if this is the correct way to do this
        Vector<double,3> v_new = sqrt(-2*kT*log(uDist(RNG))/pMass) * randomUSphere();
        Vector<double,3> pos_new = Vector<double,3>(uDist(RNG)*(area[1]-area[0])+area[0],
                                                    uDist(RNG)*(area[3]-area[2])+area[2],0);
        
        gas.R(initCount+i) = pos_new + v_new*dT;
        gas.V(initCount+i) = v_new;
        gas.M(initCount+i) = pMass;
    });
    
}