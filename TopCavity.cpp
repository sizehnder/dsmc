#include "Ippl.h"
#include "Random/Randn.h"
#include <random>
#include <vector>

#include "GasMan.h"

Vector<double,3> domainSize;
double lidSpeed = 1.0;
double T = 300.0;
double m = 6.6e-26; // Argon (kg)
std::mt19937 RNG_1(420);

// Create save-file
std::ofstream saveT("saveT.csv");
std::ofstream saveQ("saveQ.csv");

Timer timer;

void BCond(Particles& gas){
    BC_thermal(gas,0,0.0,false,k_B*T,0.0,1);                     //left
    BC_thermal(gas,0,domainSize[0],true,k_B*T,0.0,1);            //right
    BC_thermal(gas,1,0.0,false,k_B*T,0.0,0);                     //bottom
    BC_thermal(gas,1,domainSize[1],true,k_B*T,lidSpeed,0);       //top
}

void integrate(Particles& gas, uint steps, uint updates = 10, bool talk=true){
    for (uint t = 0; t < steps; t++){
        if (talk && (t*updates) % steps == 0){
            msg << t << "/" << steps << endl;
        }
        p2c2D(gas);
        moveParticles(gas);
        BCond(gas);
        gas.update();
    }
    return;
}

void takeSamples(Particles& gas, uint samples = 10, uint stepsInbetween=1){
    msg << "Start sampling" << endl;
    for (uint i = 0; i < samples; i++)
    {
        if (i % 100 == 0){msg << i << "/" << samples << endl;}
        integrate(gas,stepsInbetween,1,false);
        momInteg2D(gas,saveT,{cellLayout[0],cellLayout[1]},{domainSize[0],domainSize[1]},"temp");
        momInteg2D(gas,saveQ,{cellLayout[0],cellLayout[1]},{domainSize[0],domainSize[1]},"hFlux");
    }
    
}

// Initial setup
Particles initial(const size_t numP, Vector<double,Dim> domainSize){
    Particles gas(new Particles::Layout_t());
    gas.create(numP);
    
    std::uniform_real_distribution<double> uDist(0.0,1.0);
    
    for (size_t i = 0; i < numP; i++){ // ========================= Access elements with gas.M(i)
        gas.M(i) = m;
        gas.R(i) = Vector<double,Dim>(uDist(RNG_1), uDist(RNG_1), 0) * domainSize;  // Random initial positions (normal dist)
        gas.V(i) = (Vector<double,Dim>(uDist(RNG_1), uDist(RNG_1), 0) - Vector<double,Dim>(0.5,0.5,0)) * lidSpeed;
    }
    gas.update();
    return gas;
}


int main(int argc, char* argv[]) {

    initialize(argc, argv);
    {
        msg << "---==={ Lid-driven Cavity }===---" << endl;
        
        // #steps | n | #cells(axis) | p/cell | v_lid | temp | Kn | outMsg | #samples
        // ./TopCavity 100 1e20 100 80 100 300 1 10 0 --info 10
        
        // handle input
        const uint nt = std::atoi(argv[1]); // Number of time steps (3000 init, +samples)
        double avgNumDens = std::atof(argv[2]); // Number density (10^21 / m^3)
        cellLayout = {std::atoi(argv[3]),std::atoi(argv[3]),0}; // gridLayout (150x150)
        const uint macrosPerCell = std::atoi(argv[4]); // Avg number of macroparticles per (500)
        lidSpeed = std::atof(argv[5]);  // Lid speed (100, 300 m/s)
        T = std::atof(argv[6]); // wall temperature (300 K)
        const double Kn = std::atof(argv[7]); // Knudsen number [0.01,10]
        const int outSteps = std::atoi(argv[8]); // how many msg are passed to user
        const int samples = std::atoi(argv[9]); // No of smaples (0=none)

        sig_T = 7e-17; // Argon (~0.02eV ~ 300K) Fig.5 // https://iopscience.iop.org/article/10.1088/0953-4075/33/16/303/pdf
        double meanFreePath = 1 / (avgNumDens * sig_T);
        double L = meanFreePath / Kn;
        domainSize = {L,L,0};
        cellSize = L / cellLayout[0];
        size_t nP = macrosPerCell * cellLayout[0] * cellLayout[1];
        micPmac = avgNumDens * domainSize[0]*domainSize[1] / nP;

        if (cellSize > meanFreePath){ // If kept with 100x100 and Kn>=0.01 this will not happen
            msg << "[WARNING]: CellSize is bigger than MeanFreePath" << endl;
        }

        dT = 0.5*std::min(cellSize,meanFreePath) / std::max(sqrt(k_B*T / m),lidSpeed);
        g_max = 3*std::max(sqrt(k_B*T / m),lidSpeed);
        double nCandExp = macrosPerCell*macrosPerCell/(cellSize*cellSize) * g_max * sig_T * micPmac * dT;

        // Show user the parameters
        msg << "Time steps: " << nt << "   (dt=" << dT << ")" << endl;
        msg << "Grid: " << cellLayout << "   (cSize=" << cellSize << ")" << endl;
        msg << "DomainSize: " << domainSize << "(m)" << endl;
        msg << "#(macro)Particles: " << nP << "   (mic/mac=" << micPmac << ")" << endl;
        msg << "Temperature: " << T << "(K)" << endl;
        msg << "Lid velocity: " << lidSpeed << "(m/s)" << endl;
        msg << "Knudsen: " << Kn << "   (meanFreePath=" << meanFreePath << "(m))" << endl;
        msg << "N_cand expected: " << nCandExp << endl;
        msg << "Console updates: " << outSteps << endl;
        msg << "---==========================---" << endl;
      
        // uniform initial distribution
        Particles gas = initial(nP,domainSize);
        

        msg << "integrating..." << endl;
        timer.start();
        integrate(gas, nt, outSteps);
        timer.stop();
        msg << "...done in: " << timer.elapsed() << "(s)" << endl;
        csvExport(gas,"testExport",nP);
        timer.start();
        if (samples > 0){
            takeSamples(gas,samples,1);
        }
        timer.stop();
        msg << "DONE, total time: " << timer.elapsed() << "(s)" << endl;

        //histo(gas,20,"pos",0);
        //histo(gas,20,"pos",1);
        //histo(gas,20,"velAbs",0);
        //histo(gas,20,"velAbs",1);

    }
    finalize();

    return 0;
}
